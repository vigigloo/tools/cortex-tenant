variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "0.0.2"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "cortex_tenant_target" {
  type    = string
  default = null
}
variable "cortex_tenant_label" {
  type    = string
  default = null
}
variable "cortex_tenant_label_remove" {
  type    = bool
  default = null
}
variable "cortex_tenant_default" {
  type    = string
  default = null
}
