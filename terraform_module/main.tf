resource "helm_release" "cortex_tenant" {
  chart           = "cortex-tenant"
  repository      = "https://gitlab.com/api/v4/projects/38647140/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  dynamic "set" {
    for_each = var.cortex_tenant_target == null ? [] : [var.cortex_tenant_target]
    content {
      name  = "cortexTenant.target"
      value = var.cortex_tenant_target
    }
  }
  dynamic "set" {
    for_each = var.cortex_tenant_label == null ? [] : [var.cortex_tenant_label]
    content {
      name  = "cortexTenant.tenant.label"
      value = var.cortex_tenant_label
    }
  }
  dynamic "set" {
    for_each = var.cortex_tenant_label_remove == null ? [] : [var.cortex_tenant_label_remove]
    content {
      name  = "cortexTenant.tenant.label_remove"
      value = var.cortex_tenant_label_remove
    }
  }
  dynamic "set" {
    for_each = var.cortex_tenant_default == null ? [] : [var.cortex_tenant_default]
    content {
      name  = "cortexTenant.tenant.default"
      value = var.cortex_tenant_default
    }
  }
}
