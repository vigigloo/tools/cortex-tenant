output "internal_host" {
  value = "${var.chart_name}.${var.namespace}.svc.cluster.local"
}

output "internal_url" {
  value = "http://${var.chart_name}.${var.namespace}.svc.cluster.local:8080"
}
